document.addEventListener('init', function(event) {
  var page = event.target;

  if (page.id === 'signin') {
    page.querySelector('#push-button').onclick = function() {
      document.querySelector('#myNavigator').pushPage('login.html', {data: {title: 'Login'}});
    };
  } else if (page.id === 'login') {
    page.querySelector('ons-toolbar .center').innerHTML = page.data.title;
  }
});

function login(){
    window.location = "SplitterApp.html";
}